# linkbaum

Create a link tree that mirrors a given base folder with all content.
Directories are real, files are links.

# usage

```
 linkbaum.py [-h] [-s BASEDIR] [-t TARGETDIR] [-c CUTDIR]

Create a link tree that mirrors a given base folder with all content.
Directories are real, files are links.

optional arguments:
  -h, --help            show this help message and exit
  -s BASEDIR, --source BASEDIR
                        base folder
  -t TARGETDIR, --target TARGETDIR
                        target folder
  -c CUTDIR, --cutdir CUTDIR
                        folder in the basetree from which the target tree
                        shall start
```

E.g.: 

```
python linkbaum.py -s ./tests/testfolder/ -t ./tests/target/ -c testfolder
```
creates a linktree copy of the original tree

```
tests/
├── target
│   └── testfolder
│       ├── sub1
│       │   ├── file1 -> /home/USER/dev/linkbaum/tests/testfolder/sub1/file1
│       │   ├── file2 -> /home/USER/dev/linkbaum/tests/testfolder/sub1/file2
│       │   └── file3 -> /home/USER/dev/linkbaum/tests/testfolder/sub1/file3
│       ├── sub2
│       │   ├── file1 -> /home/USER/dev/linkbaum/tests/testfolder/sub2/file1
│       │   └── file2 -> /home/USER/dev/linkbaum/tests/testfolder/sub2/file2
│       └── sub3
│           ├── file1 -> /home/USER/dev/linkbaum/tests/testfolder/sub3/file1
│           ├── file2 -> /home/USER/dev/linkbaum/tests/testfolder/sub3/file2
│           └── file3 -> /home/USER/dev/linkbaum/tests/testfolder/sub3/file3
└── testfolder
    ├── sub1
    │   ├── file1
    │   ├── file2
    │   └── file3
    ├── sub2
    │   ├── file1
    │   └── file2
    └── sub3
        ├── file1
        ├── file2
        └── file3

```
