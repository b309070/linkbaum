import argparse
import os


def main(basedir, target, cutdir):

    for root, dirs, files in os.walk(basedir):

        rootsplit = root.split('/')
        if cutdir in rootsplit:
            idx = rootsplit.index(cutdir)
            nroot = '/'.join(rootsplit[idx:])
        else:
            nroot = root

        newdir = '{0}/{1}'.format(target, nroot)
        if not os.path.isdir(newdir):
            os.mkdir(newdir)
        for d in dirs:
            newdir = '{0}/{1}/{2}'.format(target, nroot, d)
            print("newdir is : {0}".format(newdir))
            if not os.path.isdir(newdir):
                os.mkdir(newdir)
        for f in files:
            try:
                os.symlink('{0}/{1}'.format(root, f), '{0}/{1}/{2}'.format(
                    target, nroot, f))
            except:
                pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=
        'Create a link tree that mirrors a given base folder with all content. Directories are real, files are links.'
    )
    parser.add_argument(
        '-s',
        '--source',
        dest='basedir',
        metavar='BASEDIR',
        type=str,
        nargs=1,
        help='base folder')
    parser.add_argument(
        '-t',
        '--target',
        dest='target',
        metavar='TARGETDIR',
        type=str,
        nargs=1,
        help='target folder')
    parser.add_argument(
        '-c',
        '--cutdir',
        dest='cutdir',
        metavar='CUTDIR',
        type=str,
        nargs=1,
        help='folder in the basetree from which the target tree shall start')
    args = parser.parse_args()
    basedir = os.path.abspath(args.basedir[0])
    target = args.target[0]
    cutdir = args.cutdir[0]

    if not os.path.isdir(basedir):
        raise Exception

    if not os.path.isdir(target):
        raise Exception

    if cutdir not in basedir.split('/'):
        raise Exception

    main(basedir, target, cutdir)
